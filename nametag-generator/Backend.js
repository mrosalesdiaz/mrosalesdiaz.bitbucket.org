define( [
  'dojo/_base/declare'
  , 'dojo/dom-construct'
  , 'dojo/dom-attr'
  , 'dojo/_base/lang'
  , 'dojo/_base/array'
  , 'dojo/parser'
  , 'dojo/json'
  , 'dojo/on'
  , 'dojo/query'
  , 'dojo/cookie'
  , 'dijit/_WidgetBase'
  , 'dijit/_TemplatedMixin'
  , 'dijit/_AttachMixin'
  , 'dojo/text!webapp/template.html'
  // no parameters
  , 'dojo/NodeList-dom'
  , 'dojo/NodeList-html'
  , 'dojo/domReady!'
  ], function( declare, domConstruct, domAttr, lang, array, parser, json, on, query, cookie, _WidgetBase, _TemplatedMixin, _AttachMixin, TEMPLATE_TAGNAME ) {
  declare( "PageController", [ _WidgetBase, _AttachMixin ], {
    buildRendering: function() {
      this.inherited( arguments );
      query( '[data-action]' ).on( 'click', lang.hitch( this, this.onProcess ) );
    },
    onProcess: function() {
      var lines = query( 'textarea#names' ).attr( 'value' )[ 0 ].split( '\n' );
      var question = query( 'input#question' ).attr( 'value' )[ 0 ];
      var tagWidth = parseInt( query( 'input#tagwidth' ).attr( 'value' )[ 0 ] );
      query( 'div#nametagsOutput' ).empty();
      array.forEach( lines, lang.hitch( this, function( line ) {
        var tagDiv = domConstruct.toDom( lang.replace( TEMPLATE_TAGNAME, {
          name: line,
          question: question
        } ) );
        query( 'canvas', tagDiv ).forEach( lang.partial( lang.hitch( this, this._updateImage ), line, question, tagWidth ) );
        query( tagDiv ).place( 'div#nametagsOutput' );
        console.log( arguments );
      } ) )
    },
    _updateImage: function( name, question, tagWidth, canvasElement ) {
      var constant = -1;
      var newTagHeight = 0;
      var ctx = canvasElement.getContext( "2d" );
      var img = new Image();
      img.onload = function() {
        // once the image is loaded:
        var width = img.naturalWidth; // this will be 300
        var height = img.naturalHeight; // this will be 400
        constant = height / width;
        newTagHeight = constant * tagWidth;
        query( canvasElement ).attr( {
          width: tagWidth,
          height: newTagHeight
        } );
        ctx.drawImage( img, 0, 0, tagWidth, newTagHeight );
        ctx.font = "bold 24px Century Gothic, Questrial";
      ctx.fillStyle = "red";
      ctx.fillText( "Soy", 10, 40 );
      ctx.fillStyle = "black";
      ctx.fillText( name, 30, 70 );
      ctx.font = "bold 20px Century Gothic, Questrial";
      ctx.fillStyle = "red";
      ctx.fillText( question, 10, 110 );
      };
      
      img.src = "tags-alpha-bg.png";
    },
    startup: function() {
      this.inherited( arguments );
    }
  } );
} );