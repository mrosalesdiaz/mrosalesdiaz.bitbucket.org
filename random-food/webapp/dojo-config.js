( function( global ) {
  'use strict';
  global.require = {
    async: true,
    baseUrl: '.',
    callback: function( parser ) {
      require( [ 'dojo/query', 'dojo/NodeList-dom', 'webapp/widgets' ], function( query ) {
        parser.parse().then( function() {
          query( 'body' ).attr( 'hidden', false );
        } );
      } );
    },
    deps: [ 'dojo/parser' ],
    packages: [
      {
        name: 'webapp',
        location: './webapp'
      }
    ],
    map: {
      // TodoMVC application does not use template from file system
      'dijit/_TemplatedMixin': {
        'dojo/cache': 'todo/empty'
      }
    },
    parseOnLoad: false
  };
} )( this );