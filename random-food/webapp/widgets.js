var googleApiToken = "AIzaSyAS3Zjn143AFLTQMq5sdyVla87Z9eBdgpM";
var optionsList = null;
var fileList = null;
var pageState = null;
require( [
    "dojo/_base/declare"
    , "dojo/_base/lang"
    , "dijit/_WidgetBase"
    , 'dojo/query'
    , 'dojo/request'
    , 'dojo/Evented'
    , 'dojo/on'
    , 'dojo/json'
    , 'dojo/Deferred'
    , 'dojo/dom-attr'
    , 'dojo/dom-class'
    , 'dojo/dom-construct'
    , 'dojo/io-query'
    , 'dojo/Stateful'
    , 'dojo/cookie'
    , 'dojox/mvc/getStateful'
    , 'dojox/mvc/Repeat'
    // no parameters
    , 'dojo/NodeList-dom'
], function( declare, lang, _WidgetBase, query, request, Evented, on, json, Deferred, domAttr, domClass, domConstruct, ioQuery, Stateful, cookie, getStateful, Repeat ) {
  pageState = new Stateful( {
    radius: 5
  } );
  optionsList = getStateful( {
    "identifier": "Serial",
    "items": []
  } );
  var fnDeclareWidget = function( fnName, fnBody, parentClasses ) {
    declare( fnName, parentClasses || [ _WidgetBase, Evented ], fnBody );
  };
  fnDeclareWidget( "webapp.components.globalEvent", {
    startup: function() {
      this.emit( 'page-loaded' );
    },
    registerCheckBoxes: function() {
      query( '.webapp-options' ).on( 'click', lang.hitch( this, this.__optionsChanged ) );
    },
    __optionsChanged: function() {
      this.emit( 'options-changed', query( '.webapp-options' ).some( function( elem ) {
        return elem.checked;
      } ) );
    },
    fireOptionsChanged: function() {
      this.__optionsChanged();
    },
    setRandomRestaurant: function() {
      var indexes=query( '.webapp-options' ).filter(function  (elem) {
        return elem.checked;
      }).map( function( elem ) {
        return elem.value;
      } )

      var random = function( min, max ) {
        return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
      }
      var restaurantIndex = parseInt(indexes[ random( 0, indexes.length - 1 ) ]);
      var restaurant=optionsList.items[restaurantIndex];
      pageState.set( "restaurantName", restaurant.name );
      pageState.set( "restaurantDetail", restaurant.detail );
      pageState.set( "restaurantIcon", restaurant.icon );
      pageState.set( "restaurantPhoto", restaurant.photo );
    }
  } );
  fnDeclareWidget( "webapp.buttons.goStepTwo", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__click ) )
    },
    __checkEnable: function() {},
    __click: function() {
      pageEvents.emit( "second-step" );
      query( "#first-step" ).addClass( "hide" );
      query( "#second-step" ).removeClass( "hide" );
      query( "#third-step" ).addClass( "hide" );
    }
  } );
  fnDeclareWidget( "webapp.buttons.reset", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__click ) )
    },
    __checkEnable: function() {},
    __click: function() {
      pageEvents.emit( "first-step" );
      query( "#first-step" ).removeClass( "hide" );
      query( "#second-step" ).addClass( "hide" );
      query( "#third-step" ).addClass( "hide" );
    }
  } );
  fnDeclareWidget( "webapp.buttons.goStepThree", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__click ) )
    },
    startup: function() {
      this.inherited( arguments );
      on( pageEvents, 'options-changed', lang.hitch( this, this.__checkEnable ) );
    },
    __checkEnable: function( atLeastOne ) {
      if ( atLeastOne ) {
        domClass.remove( this.domNode, 'disabled' )
      } else {
        domClass.add( this.domNode, 'disabled' )
      }
    },
    __click: function() {
      pageEvents.emit( "third-step" );
      query( "#first-step" ).addClass( "hide" );
      query( "#second-step" ).addClass( "hide" );
      query( "#third-step" ).removeClass( "hide" );
      pageEvents.setRandomRestaurant();
    }
  } );
  fnDeclareWidget( "webapp.buttons.goStepOne", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__click ) )
    },
    __click: function() {
      query( "#first-step" ).removeClass( "hide" )
      query( "#second-step" ).addClass( "hide" )
      query( "#third-step" ).addClass( "hide" )
    }
  } );
  fnDeclareWidget( "webapp.buttons.currentPosition", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__click ) )
    },
    __click: function() {
      pageEvents.emit( 'current-position' );
    }
  } );
  fnDeclareWidget( "webapp.buttons.selectall", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__selectAllOptions ) );
    },
    __selectAllOptions: function() {
      query( '.webapp-options' ).forEach( function( elem ) {
        elem.checked = true;
      } );
      pageEvents.fireOptionsChanged();
    }
  } );
  fnDeclareWidget( "webapp.buttons.invert", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__selectAllOptions ) );
    },
    __selectAllOptions: function() {
      query( '.webapp-options' ).forEach( function( elem ) {
        elem.checked = !elem.checked;
      } );
      pageEvents.fireOptionsChanged();
    }
  } );
  fnDeclareWidget( "webapp.buttons.again", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'click', lang.hitch( this, this.__click ) )
    },
    __click: function() {
      pageEvents.setRandomRestaurant();
    }
  } );
  fnDeclareWidget( "webapp.selects.radius", {
    buildRendering: function() {
      this.inherited( arguments );
      on( this.domNode, 'change', lang.hitch( this, this.__onchange ) );
    },
    startup: function() {      
      this.inherited( arguments );
      on( pageEvents, 'page-loaded', lang.hitch( this, this.__fireChanged ) )
    },
    __fireChanged: function() {
      this.__onchange( {
        srcElement: this.domNode
      } )
    },
    __onchange: function( _event ) {
      pageState.set( "radius", parseInt( _event.srcElement.value ) );
      pageEvents.emit( "change-radius", parseInt( _event.srcElement.value ) );
    }
  } );
  fnDeclareWidget( "webapp.selects.options", {
    buildRendering: function() {
      this.inherited( arguments );
    }
  } );
  fnDeclareWidget( "webapp.external.gmap", {
    __dafaultGMapParameters: {
      center: new google.maps.LatLng( -12.0931317, -77.0026428 ),
      zoom: 17,
      scrollwheel: false
    },
    __image: {
      url: 'img/foods-icon.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size( 16, 16 ),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point( 0, 0 ),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point( 8, 8 )
    },
    __markers: [],
    __calculateCurrentPosition: function() {
      var promisse = new Deferred();
      navigator.geolocation.getCurrentPosition( function( position ) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        promisse.resolve( pos );
      }, promisse.reject );
      return promisse;
    },
    __updateMapCenter: function( pos ) {
      this.gmap.setCenter( new google.maps.LatLng( pos.lat, pos.lng ) );
    },
    __createGMap: function() {
      query( "#gmap" ).forEach( lang.hitch( this, function( elem ) {
        this.gmap = new google.maps.Map( elem, this.__dafaultGMapParameters );
        this.__infoWindow = new google.maps.InfoWindow( {
          content: '...'
        } );
      } ) )
    },
    __updateCenterInPageState: function() {
      pageState.set( "center", {
        lat: this.gmap.getCenter().lat(),
        lng: this.gmap.getCenter().lng()
      } );
    },
    __showPopup: function( place, marker ) {
      this.__infoWindow.open( this.gmap );
      this.__infoWindow.setContent( place.name );
      this.__infoWindow.setPosition( marker.getPosition() );
    },
    __createMarker: function( place ) {
      var photo = '';
      if ( place.photos !== undefined && place.photos.length > 0 ) {
        photo = place.photos[ 0 ].getUrl( {
          'maxWidth': 64,
          'maxHeight': 64
        } );
      } else {
        photo = place.icon;
      }
      // If the request succeeds, draw the place location on
      // the map as a marker, and register an event to handle a
      // click on the marker.
      var marker = new google.maps.Marker( {
        map: this.gmap,
        icon: this.__image,
        title: place.name,
        position: place.geometry.location
      } );
      google.maps.event.addListener( marker, 'click', lang.partial( lang.hitch( this, this.__showPopup ), place, marker ) );
      this.__markers.push( marker );
      optionsList.items.push( {
        name: place.name,
        id: place.id,
        place_id: place.place_id,
        icon: place.icon,
        detail: place.vicinity,
        photo: photo
      } );
    },
    __queryPlaces: function() {
      query( "#btnGoStepTwo" ).addClass( 'disabled' );
      var request = {
        location: this.gmap.getCenter(),
        radius: pageState.get( "radius" ),
        types: [ 'restaurant' ]
      };
      this.__markers.forEach( function( marker ) {
        marker.setMap( null );
      } );
      this.__markers = [];
      while(optionsList.items.length>0)
      optionsList.items.shift();
      var service = new google.maps.places.PlacesService( this.gmap );
      service.nearbySearch( request, lang.hitch( this, function( results, status, pages ) {
        if ( status == google.maps.places.PlacesServiceStatus.OK ) {
          results.forEach( lang.hitch( this, this.__createMarker ) );
          if ( pages.hasNextPage ) {
            pages.nextPage();
          } else {
            query( "#btnGoStepTwo" ).removeClass( 'disabled' );
            console.log( this.__markers.length )
            pageEvents.registerCheckBoxes();
          }
        }
      } ) );
    },
    __setCrosshair: function() {
      var htmlCrossHair = "<img src=\"img/crosshairs.gif\" class=\"\" style=\"-webkit-transform: translate(-50%, -50%); width: 19px; height: 19px; border: 0px; position: relative; top: 50%; left: 50%; z-index: 500;\">";
      query( "#gmap" ).addContent( htmlCrossHair, 'last' )
    },
    __showCurrentPosition: function() {
      this.__calculateCurrentPosition().then( lang.hitch( this, this.__updateMapCenter ) );
    },
    startup: function() {
      this.inherited( arguments );
      this.__createGMap();
      this.__showCurrentPosition();
      on( pageEvents, 'second-step', lang.hitch( this, this.__updateCenterInPageState ) );
      on( pageEvents, 'change-radius', lang.hitch( this, this.__queryPlaces ) );
      this.__setCrosshair();
      on( pageEvents, 'current-position', lang.hitch( this, this.__showCurrentPosition ) )
    }
  } );
} );