var gulp = require('gulp');
var webserver = require('gulp-webserver');
 
gulp.task('webserver', function() {
  gulp.src(__dirname)
    .pipe(webserver({
      port:8000,
      https:true,
      //livereload: true,
      directoryListing: true,
      open: true
    }));
});