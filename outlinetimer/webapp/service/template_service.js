define( [ 'dojo/store/JsonRest', 'module' ], function( JsonRest, module ) {
  module.setExports( new JsonRest( {
    target: '<url>/comments',
    idProperty: 'id',
    headers: {}
  } ) );
} );