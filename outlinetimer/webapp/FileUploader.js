/***************************************
 * File: PageAlerts.js
 *
 * Notification service for the page. 
 * It listens for events and trigger 
 * notifications(alerts,etc)
 *
 * <code>
 *    AppEvents.emit('page-info',{message:"Hello"});
 * </code>
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/query',
  'dojo/on',
  'dojo/Deferred',
  'dojo/promise/all',
  'dojo/domReady!'
], function(declare, lang, query, on, Deferred, all) {
  /****** Controller *****/
  var _controller = {
    uploadFileFromInput: function(inputFileOrSelector, service, fnData) {
      var defer = new Deferred();
      var inputFilesElements = null;
      var files = [];

      if (lang.isString(inputFileOrSelector)) {
        inputFilesElements = query(inputFileOrSelector);
      } else {
        inputFilesElements = [inputFileOrSelector];
      };

      if (inputFilesElements.length == 0) {
        defer.reject("No inputs to process.");
      } else {
        for (var i = 0; i < inputFilesElements.length; i++) {
          if (inputFilesElements[i].files.length == 0) {
            defer.reject("No Files to process.");
          } else {
            for (var j = 0; j < inputFilesElements[i].files.length; j++) {
              files.push(inputFilesElements[i].files[j])
            }
          }
        }

        var promisses = [];

        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          promisses.push(this.__uploadFile(service, fnData, file));
        }

        all(promisses).then(lang.partial(function(defer, data) {
          defer.resolve(data);
        }, defer), lang.partial(function(defer, err) {
          defer.reject(err)
        }, defer));

      }


      return defer.promise;
    },
    __uploadFile: function(service, fnData, file) {
      var defer = new Deferred();

      var dataToUpload = fnData.apply(null, [file]);
      var reader = new FileReader();
      reader.onload = lang.partial(function(service, data, e) {
        console.log(data);
        console.log(service);

        data.data = data.data || {};
        // data.options.headers = {
        //   "Content-Type": "application/vnd.api+json"
        // };
        data.data[data.fileProperty] = e.currentTarget.result;
        service.put(data.data, data.options).then(function(data) {
          defer.resolve(data);
        }, function(err) {
          defer.reject(err);
        });
      }, service, dataToUpload)

      reader.readAsDataURL(file);
      return defer.promise;
    }
  };
  // Register Global Events
  AppEvents.registerDefaultEvents(_controller);
  // AppEvents.on('event-name', lang.hitch( _controller, ( _controller.fnName || function() {} ) ) );
  /***********************/
  return _controller;
});