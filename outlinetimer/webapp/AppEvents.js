/***************************************
 * File: AppEvents.js
 *
 * Global application object to share information 
 * with other classes and handles event mechanism 
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/on',
  'dojo/Evented',
  'dojo/query',
  'dojo/Deferred',
  'module',
  'dojox/mvc/getStateful',
  'dojo/io-query',
  'dojo/NodeList-dom'
], function(declare, lang, on, Evented, query, Deferred, module, getStateful, ioQuery) {

  var parts = location.href.split('?');
  var queryString = parts.length == 2 ? parts.pop() : "";

  return declare(Evented, {
    queryParams: ioQuery.queryToObject(queryString),
    constructor: function() {
      this.inherited(arguments);
      on(this, 'page-loaded', lang.hitch(this, this.__onPageLoaded));
      on(this, 'page-error', lang.hitch(this, this.__onPageError));
    },
    startup: function() {
      this.emit('loaded', this.params);
      this.inherited(arguments);
    },
    ask: function(message) {
      var defer = new Deferred();
      if (confirm(Array.prototype.join.call(arguments, " "))) {
        defer.resolve();
      } else {
        defer.reject();
      }
      return defer.promise;
    },
    registerDefaultEvents: function(controller) {
      // register startup in controller
      on(this, 'loaded', lang.hitch(controller, (controller.startup || function() {})));
    },
    __onPageLoaded: function() {
      query('body').attr('hidden', false);
    },
    __onPageError: function(err) {
      PageAlerts.model.set("title", err.title || "Webapp");
      PageAlerts.model.set("message", err.message || "Application Error, message empty");
    }
  });
});