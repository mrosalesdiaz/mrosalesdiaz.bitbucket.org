/***************************************
 * File: Paginator.js
 *
 * Paginatior object
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/Evented',
  'dojo/query',
  'dojo/NodeList-dom'
], function(declare, lang, Evented, query) {
  return declare([], {
    constructor: function(pageSize) {
      this.inherited(arguments);
      this._disableNext = true;
      this._disablePrevious = true;
      this.DEFAULT_PAGE_SIZE = pageSize || 12;
      this.index = 0;
      this.pageSize = this.DEFAULT_PAGE_SIZE;
      this.totalPages = -1;
      console.log("aa")
    },
    startup: function() {
      query('.pagination-previous').on('click', lang.partial(lang.hitch(this, this.__processPaginationChange), -1))
      query('.pagination-next').on('click', lang.partial(lang.hitch(this, this.__processPaginationChange), +1))
    },
    _updateButtonsDisabling: function() {
      this._disableNext = (this.index >= this.totalPages);
      this._disablePrevious = (this.index === 0);

      if (this._disablePrevious) {
        query('.pagination-previous').addClass('disabled');
      } else {
        query('.pagination-previous').removeClass('disabled');

      }
      if (this._disableNext) {
        query('.pagination-next').addClass('disabled');
      } else {
        query('.pagination-next').removeClass('disabled');

      }
    },
    firstPage: function() {
      AppEvents.emit('pagination-change', {
        direction: 0,
        skip: this.pageSize * this.index,
        limit: this.pageSize,
        event: null
      })
    },
    lastPage: function() {
      AppEvents.emit('pagination-change', {
        direction: 0,
        skip: this.pageSize * (this.totalPages - 1) - 1,
        limit: this.pageSize,
        event: _event
      })
    },
    updateTotal: function(xhr) {
      var total = xhr.getResponseHeader("X-Content-Range");
      try {
        total = (total = total.match(/\/(.*)/)) && +total[1];
      } catch (e) {
        total = -1;
      }
      this.totalPages = Math.floor(total / this.pageSize);
      this._updateButtonsDisabling();
    },
    reload: function() {
      AppEvents.emit('pagination-change', {
        direction: 1,
        skip: this.pageSize * this.index,
        limit: this.pageSize,
        event: null
      })
    },
    __processPaginationChange: function(direction, _event) {
      if ((direction > 0 && this._disableNext) || direction < 0 && this._disablePrevious) {
        return;
      }
      this.index = this.index + direction;
      AppEvents.emit('pagination-change', {
        direction: direction,
        skip: this.pageSize * this.index,
        limit: this.pageSize,
        event: _event
      })
    }
  });
});