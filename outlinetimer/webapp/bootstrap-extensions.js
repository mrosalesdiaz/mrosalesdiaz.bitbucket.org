/***************************************
 * File: bootstrap-extensions.js
 *
 * Extensions for add functionalities to bootstrap widget
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
$(function() {
	// customization to hide and not remove alerts when are dismissed
	$("[data-hide]").on("click", function() {
		$("." + $(this).attr("data-hide")).hide();
		// -or-, see below
		// $(this).closest("." + $(this).attr("data-hide")).hide();
	});

	// customization to trigger change events 
	// when data in a contenteditable=true element has changed
	$('body').on('focus', '[contenteditable]', function() {
		var $this = $(this);
		$this.data('before', $this.html());
		AppEvents.emit('bug-data',this);
		return $this;
	}).on('blur keyup paste input', '[contenteditable]', function() {
		var $this = $(this);
		if ($this.data('before') !== $this.html()) {
			$this.data('before', $this.html());
			$this.trigger('change');
			AppEvents.emit('bug-data',this);
		}
		return $this;
	});
});