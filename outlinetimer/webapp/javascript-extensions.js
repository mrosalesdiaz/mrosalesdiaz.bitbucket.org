/***************************************
 * File: javascript-extensions.js
 *
 * Additional JavaScript functions to reduce code.
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
(function(global) {
  'use strict';
  /**
   * Model class to convert an object in to other 
   * with only some attributes and support attribute 
   * renaming.
   * <code>
   *    var __model={name:"...",test:null,a:3};
   *    var param = new Model(__model)
   *                .name() // get attribute
   *                .test("default")  // with default if null
   *                .a(null,"new_name") // renaming 
   *                .build(); 
   * </code>
   *
   **/
  if (!global.QueryParam) {
    global.QueryParam = function(model) {
      this.__model = model;
    };
    global.QueryParam.prototype.toQueryString = function() {
      var query = "?";
      for (var s in this.__model) {
        query += s + "=" + JSON.stringify(this.__model);
      }
      return query;
    };
  }
  /**
   * Model class to convert an object in to other 
   * with only some attributes and support attribute 
   * renaming.
   * <code>
   *    var __model={name:"...",test:null,a:3};
   *    var param = new Model(__model)
   *                .name() // get attribute
   *                .test("default")  // with default if null
   *                .a(null,"new_name") // renaming 
   *                .build(); 
   * </code>
   *
   **/
  if (!global.Model) {
    global.Model = function(model) {
      this.model = model;
      this._tempModel = {};
      for (var propName in this.model) {
        this[propName] = function(propName, defaultValue, renameProp) {
          this._tempModel[renameProp || propName] = this.model[propName] || defaultValue;
          return this;
        }.bind(this, propName);
      }
      this.build = function() {
        return this._tempModel;
      }
    };
  }
  // Clear the array using pop function in other to support
  // MVC listeners
  if (!Array.prototype.clear) {
    Array.prototype.clear = function() {
      while (this.length) {
        this.pop();
      };
    };
  };


  // to get index using fn to search
  /*if (!Array.prototype.indexOf)*/
  {
    var _old = Array.prototype.indexOf;
    Array.prototype.indexOf = function(fn) {
      var index = -1
      if (typeof fn === 'function') {
        var index = [];
        for (var i = 0; i < this.length; i++) {
          if (fn.apply(this, [this[i]])) {
            index = i;
          }
        }
      } else {
        index = _old.apply(this, [fn]);
      }
      return index;  
    };
  };
  // Replaces the array all items (clear and push each)
  // in order to support MVC listeners
  if (!Array.prototype.replace) {
    Array.prototype.replace = function(data) {
      this.clear();
      data.forEach(function(elem) {
        this.push(elem);
      }.bind(this));
    };
  };
  // Checks if array contains an element using function to test
  if (!Array.prototype.contains) {
    Array.prototype.contains = function(fn) {
      return this.filter(fn).length === 0;
    };
  };
  // Move the element up in order if possible
  if (!Array.prototype.moveIndexDown) {
    Array.prototype.moveIndexDown = function(fn) {
      var indexes = [];
      for (var i = 0; i < this.length; i++) {
        if (fn.apply(null, [this[i]])) {
          indexes.push(i);
        }
      }

      for (var i = 0; i < indexes.length; i++) {
        var tempData = this[indexes[i]];
        this.splice(indexes[i], 1);
        this.splice(Math.max(indexes[i] - 1, 0), 0, tempData);
      };
      return this;
    };
  };
  // Move the element down in order if possible
  if (!Array.prototype.moveIndexUp) {
    Array.prototype.moveIndexUp = function(fn) {
      var indexes = [];
      for (var i = 0; i < this.length; i++) {
        if (fn.apply(null, [this[i]])) {
          indexes.push(i);
        }
      }
      for (var i = 0; i < indexes.length; i++) {
        var tempData = this[indexes[i]];
        this.splice(indexes[i], 1);
        this.splice(Math.min(indexes[i] + 1, this.length), 0, tempData);
      };
      return this;
    };
  };
  // Removes element supporting Dojo MVC listeners
  if (!Array.prototype.remove) {
    Array.FIND_ID = function(id) {
      return function(paramId, elem) {
        return paramId === elem.id;
      }.bind(null, id);
    }
    Array.prototype.remove = function(fn) {
      var indexes = [];
      for (var i = 0; i < this.length; i++) {
        if (fn.apply(null, [this[i]])) {
          indexes.push(i);
        }
      }
      for (var i = indexes.length - 1; i >= 0; i--) {
        this.splice(indexes[i], 1);
      };
      return this;
    };

  };
  // truncate a string, 'addi...'
  if (!String.prototype.truncate) {
    // TODO: improve to support many separators
    String.prototype.truncate = function(n, useWordBoundary, separator) {
      var isTooLong = this.length > n,
        s_ = isTooLong ? this.substr(0, n - 1) : this;
      s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(separator || ' ')) : s_;
      return isTooLong ? s_ + '&hellip;' : s_;
    };
  };
  // truncate a string by left '...left'
  if (!String.prototype.truncateLeft) {
    // TODO: improve to support many separators
    String.prototype.truncateLeft = function(n, useWordBoundary, separator) {
      var isTooLong = this.length > n,
        s_ = isTooLong ? this.substr(this.length - n) : this;
      s_ = (useWordBoundary && isTooLong) ? this.substr(this.substr(0, this.length - s_.length).lastIndexOf(separator || ' ')) : s_;
      return isTooLong ? '&hellip;' + s_ : s_;
    };
  };
  // check is string is null or empty
  if (!String.prototype.isNullOrEmpty) {
    String.prototype.isNullOrEmpty = function() {
      return this.trim() === '';
    };
    String.isNullOrEmpty = function(obj) {
      return obj === undefined || obj === null || obj.trim() === '';
    };
  };
  // trim string
  if (!String.prototype.trim) {
    String.prototype.trim = function() {
      return this.replace(/^\s+|\s+$/gm, '');
    };
  };
})(this);