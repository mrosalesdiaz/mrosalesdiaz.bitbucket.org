define( [
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojox/mvc/getStateful',
  'dojox/mvc/StatefulArray',
  'dojo/domReady!'
], function( declare, lang, getStateful, StatefulArray ) {
  /****** Controller *****/
  var _controller = {
    model: getStateful( {
      // simpleText: 'Default Text'
    } ),
    list: new StatefulArray([]),
    startup: function() {
      AppEvents.emit( 'page-loaded' );
    },
    process: function() {
      // this.model.set( 'prop', 'value' );
    }
  };
  // Register Global Events
  AppEvents.registerDefaultEvents( _controller );
  // AppEvents.on('event-name', lang.hitch( _controller, ( _controller.fnName || function() {} ) ) );
  /***********************/
  return _controller;
} );