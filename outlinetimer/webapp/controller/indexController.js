define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/query',
  'dojo/Deferred',
  'dojox/mvc/getStateful',
  'dojox/mvc/StatefulArray',
  'dojox/store/db/IndexedDB',
  'Circliful',
  'dojo/NodeList-html',
  'dojo/NodeList-dom',
  'dojo/domReady!'
], function(declare, lang, query, Deferred, getStateful, StatefulArray, IndexedDB) {
  var CLASS_SIDEBAR_SELECTED = "list-group-item-success";
  var FORMAT_TWO_00 = function(num) {
    return ("" + (100 + num)).substring(1);
  };
  var GET_MINUTES = function(val) {
    return Math.floor(val / 60);
  };
  var GET_SECONDS = function(val) {
    return val % 60;
  };
  /****** Controller *****/
  var _controller = {
    model: getStateful({
      totalSecondsTime: 0,
      labelTotalTime: "00:00",
      percentTotalProgress: 0,

      sectionSecondsTime: 0,
      labelSectionTime: "00:00",
      percentSectionProgress: 0,

      currentSectionIndex: 0,
      currentSectionTitle: "[empty]",

      ///////////////////
      textToParse: "1. titulo #1 (1m)\n2. titulo #2 (2m)\n3. titulo #3 (3m)"
    }),
    __db: new IndexedDB({
      dbConfig: {
        version: 1, // this is required
        stores: {
          outline: {
            data: {}
          }
        }
      },
      storeName: 'outline'
    }),
    listTitles: new StatefulArray([]),
    __timer: null,
    startup: function() {
      // register property change listeners
      this.model.watch("totalSecondsTime", lang.hitch(this, this.__updateTotalTimeTextAndPercent));
      this.model.watch("sectionSecondsTime", lang.hitch(this, this.__updateSectionTimeTextAndPercent));

      this.__db.clear = function() {
        var defer = new Deferred();
        this.query({}).then(lang.hitch(this, function(data) {
          if (data == null || data.length == 0) {
            defer.resolve();
          } else {
            data.forEach(lang.hitch(this, function(elem) {
              this.remove(elem.id).then(function() {
                defer.resolve();
              });
            }));
          }
        }));
        return defer.promise;
      };
      this.__loadOutlineData().then(lang.hitch(this, function() {
        this.__createRing()
        AppEvents.emit('page-loaded');
      }));
    },
    __updateTotalTimeTextAndPercent: function() {
      this.model.set("labelTotalTime", FORMAT_TWO_00(GET_MINUTES(this.model.totalSecondsTime)) + ":" + FORMAT_TWO_00(GET_SECONDS(this.model.totalSecondsTime)));
      this.model.set("percentTotalProgress", Math.floor(100 * this.__getSumOfRestTime() / this.__getSumOfAllTime()));
      query("#progressBar>.progress-bar").html(lang.replace("{labelTotalTime}", this.model));
      query("#progressBar>.progress-bar").style("width", this.model.percentTotalProgress + "%");
      query("#progressBar>.progress-bar").attr("aria-valuenow", this.model.percentTotalProgress);
    },
    __updateSectionTimeTextAndPercent: function() {
      this.model.set("labelSectionTime", FORMAT_TWO_00(GET_MINUTES(this.model.sectionSecondsTime)) + ":" + FORMAT_TWO_00(GET_SECONDS(this.model.sectionSecondsTime)));
      this.model.set("percentSectionProgress", Math.floor(100 * this.model.sectionSecondsTime / this.listTitles[this.model.currentSectionIndex].seconds));
      this.__updateRingPercent(this.model.percentSectionProgress);
      this.__updateRingText(this.model.labelSectionTime);
    },
    __createRing: function() {
      $("#test-circle").circliful({
        animationStep: 5,
        foregroundBorderWidth: 12,
        backgroundBorderWidth: 15,
        backgroundColor: '#ebebeb',
        foregroundColor: '#337ab7',
        percent: 0,
        replacePercentageByText: "00:00"
      });
    },
    __updateRingPercent: function(percent) {
      $('#test-circle .circle').attr("stroke-dasharray", "" + (360 * percent / 100) + ", 20000");
    },
    __updateRingText: function(text) {
      $('#test-circle tspan.number').html(text);
    },
    parseText: function() {
      var text = this.model.get("textToParse");
      var data = text.split("\n").map(lang.hitch(this, function(line) {
        var time = line.substring(line.lastIndexOf("(") + 1);
        time = time.substring(0, time.lastIndexOf(")")).replace(/(\ )/ig, "");
        var title = line.substring(0, line.lastIndexOf("(") - 1).trim();

        return {
          title: title,
          time: time,
          seconds: this.__parseTime(time) * 60
        }
      }));
      this.__db.clear().then(lang.hitch(this, function() {
        this.__db.put({
          data: data,
          id: 1
        }, {
          id: 1
        }).then(lang.hitch(this, this.__loadOutlineData));
      }));

      $("#modalParser").modal("hide")
    },
    __loadOutlineData: function() {
      var defer = new Deferred()
      this.__db.get(1).then(lang.hitch(this, function(row) {
        if (row) {
          this.listTitles.replace(row.data);
        };
        defer.resolve();

      }));
      return defer.promise;
    },
    __parseTime: function(timeText) {
      var minutes = parseInt(timeText);
      return minutes;
    },
    __updateCurrentSectionData: function() {
      var data = this.listTitles[this.model.currentSectionIndex];
      this.model.set("currentSectionTitle", data.title);
      this.model.set("sectionSecondsTime", data.seconds);
      this.model.set("totalSecondsTime", this.__getSumOfRestTime());
    },
    __startTimer: function() {
      this.__timer = window.setInterval(lang.hitch(this, this.__update), 1000);
    },
    __stopTimer: function() {
      window.clearInterval(this.__timer);
      this.__timer = null;
    },
    __getSumOfRestTime: function() {
      var total = 0;
      for (var i = this.model.currentSectionIndex + 1; i < this.listTitles.length; i++) {
        total += this.listTitles[i].seconds;
      }
      return total + this.model.sectionSecondsTime;
    },
    __getSumOfAllTime: function() {
      var total = 0;
      for (var i = 0; i < this.listTitles.length; i++) {
        total += this.listTitles[i].seconds;
      }
      return total;
    },
    __update: function() {
      window.setTimeout(lang.hitch(this, function() {
        if (this.model.sectionSecondsTime > 0) {
          this.__reduceSectionTime();
          this.__reduceTotalTime();
        } else {
          this.__moveNext();
        }
      }), 0);
    },
    __updateSectionSelectedInSidebar: function() {
      query(".outline-row").removeClass(CLASS_SIDEBAR_SELECTED);
      query(".outline-row-" + this.model.currentSectionIndex).addClass(CLASS_SIDEBAR_SELECTED);
    },
    __moveNext: function() {
      var newIndex = this.model.currentSectionIndex + 1;
      if (newIndex < this.listTitles.length) {
        this.model.set("currentSectionIndex", newIndex);
        this.__updateCurrentSectionData();
        this.__updateSectionSelectedInSidebar();
      } else {
        this.__finished();
      }
    },
    __finished: function() {
      window.clearInterval(this.__timer);
      this.model.set("currentSectionIndex", 0);
      this.model.set("currentSectionTitle", "[finished]");
    },
    __reduceSectionTime: function() {
      this.model.set("sectionSecondsTime", this.model.sectionSecondsTime - 1);
    },
    __reduceTotalTime: function() {
      this.model.set("totalSecondsTime", this.__getSumOfRestTime());
    },
    showParseDialog: function() {
      $("#modalParser").modal("show");
    },
    playTimer: function() {
      if (this.__timer == null) {
        if (this.__paused) {
          this.__paused = false;
        } else {
          this.__updateCurrentSectionData();
        }
        this.__updateSectionSelectedInSidebar();
        this.__startTimer();
      } else {
        AppEvents.emit("page-error", {
          message: "Error: Doble timer"
        })
      }

    },
    pauseTimer: function() {
      this.__paused = true;
      this.__stopTimer();
    },
    restartTimer: function() {
      this.model.set("currentSectionIndex", 0);
      this.__stopTimer();
      this.__updateCurrentSectionData();
      this.__updateSectionSelectedInSidebar();
      this.__startTimer();
    },
    selectSection: function(index) {
      this.model.set("currentSectionIndex", index);
      this.__stopTimer();
      this.__updateCurrentSectionData();
      this.__updateSectionSelectedInSidebar();
      this.__startTimer();
    }


  };
  // Register Global Events
  AppEvents.registerDefaultEvents(_controller);
  // AppEvents.on('event-name', lang.hitch( _controller, ( _controller.fnName || function() {} ) ) );
  /***********************/
  return _controller;
});