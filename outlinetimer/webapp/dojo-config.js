/***************************************
 * File: dojo-config.js
 *
 * Dojo configuration files, it registers 
 * global classes and load external libs also, 
 * the one does not support direct load.
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
(function(global) {
  'use strict';
  var BASE_PATH = location.href.split("/").slice(0, -2).join("/");
  global.require = {
    async: true,
    baseUrl: '.',
    callback: function(parser) {
      require(['dojo/query', 'webapp/AppEvents', 'dojo/NodeList-dom', 'webapp/javascript-extensions', 'webapp/bootstrap-extensions', 'webapp/Reform', 'webapp/dojo-shortcuts'], function(query, AppEvents) {
        global.AppEvents = new AppEvents();
        parser.parse().then(function() {
          global.AppEvents.startup();
        });
      });
    },
    deps: ['dojo/parser'],
    packages: [{
      name: 'webapp',
      location: BASE_PATH+'/webapp'
    }, {
      name: 'bootstrap-extensions',
      location: BASE_PATH+'/webapp/'

    }, {
      name: 'javascript-extensions',
      location: BASE_PATH+'/webapp/'
    }, {
      name: 'dojo-shortcuts',
      location: BASE_PATH+'/webapp/'
    }, {
      name: 'reveal-utils',
      location: BASE_PATH+'/webapp/'
    }, {
      name: 'Reform',
      location: BASE_PATH+'/webapp/'
    }, {
      name: 'Reveal',
      location: BASE_PATH+'/reveal/js',
      main: 'reveal'
    }, {
      name: 'Reveal-Headead',
      location: BASE_PATH+'/reveal/lib/js',
      main: 'head.min'
    }, {
      name: 'Circliful',
      location: BASE_PATH+'/circliful/js',
      main: 'jquery.circliful.min'
    }],
    map: {
      // TodoMVC application does not use template from file system
      'dijit/_TemplatedMixin': {
        'dojo/cache': 'todo/empty'
      }
    },
    parseOnLoad: false,
  };
})(this);