/***************************************
 * File: PostForm.js
 *
 * Class to execute a form post/get to 
 * send data to server and also redirect.
 *
 * <code type="js-dojo">
 *    var postData = {name:"hello",age:2};
 *    PostForm.submit(postData, '/fbauth');
 * </code>
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/json',
  'dojo/on',
  'dojo/query',
  // no parameters
  'dojo/domReady!'
], function(declare, lang, array, domConstruct, domAttr, json, on, query) {

  return {
    TEMPLATE: '<form></form>',
    __init: function() {
      // TODO: require to implement one only form element by page 
      this.domNode = domConstruct.place(this.TEMPLATE, document.body);
    },
    __setAttributes: function(action, method, target) {
      action = action || '';
      method = method || 'POST';
      target = target || '_self';

      domAttr.set(this.domNode, 'action', action);
      domAttr.set(this.domNode, 'method', method);
      domAttr.set(this.domNode, 'target', target);
    },
    submit: function(data, action, method, target) {
      this.__init();
      this.__setAttributes(action, method, target)
      var dataToPost = [];
      domConstruct.empty(this.domNode);
      for (var key in data) {
        var val = data[key];
        if (lang.isArray(val)) {
          for (var i = 0; i < val.length; i++) {
            var valInArray = val[i];
            if (lang.isArray(valInArray) || lang.isObject(valInArray)) {
              valInArray = JSON.stringify(valInArray);
            };
            dataToPost.push({
              name: key,
              value: valInArray
            });
          }
        } else if (lang.isObject(val)) {
          dataToPost.push({
            name: key,
            value: JSON.stringify(val)
          });
        } else {
          dataToPost.push({
            name: key,
            value: val
          });
        }
      };
      for (var i = 0; i < dataToPost.length; i++) {
        domConstruct.place(lang.replace('<input type="hidden" value="{value}" name="{name}"/>', dataToPost[i]), this.domNode);
      }
      this.domNode.submit();
    }
  };
});