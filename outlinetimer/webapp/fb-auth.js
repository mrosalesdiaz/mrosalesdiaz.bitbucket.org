/***************************************
 * File: fb-auth.js
 *
 * Function for Facebook authentication
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
window.fbAsyncInit = function() {
  FB.init({
    appId: '269648420139185',
    cookie: true,
    xfbml: true,
    version: 'v2.8'
  });
  FB.AppEvents.logPageView();
  FB.getLoginStatus(function(response) {
    controller.validateAccess(response);
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


function checkLoginState() {
  FB.getLoginStatus(function(response) {
    controller.validateAccess(response);
  });
}