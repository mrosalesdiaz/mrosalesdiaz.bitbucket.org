/***************************************
 * File: Validators.js
 *
 * Utility validations function 
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/lang'
], function(lang) {
  var _instance = {
    isDuration: function(val) {
      return this.isNumber(val) && this.isNonZero(val) && this.isPositive(val);
    },
    isNumber: function(val) {
      return !isNaN(parseFloat(val)) && isFinite(val);
    },
    isNonZero: function(val) {
      return !isNaN(val) && parseFloat(val) !== 0;
    },
    isPositive: function(val) {
      return !isNaN(val) && parseFloat(val) > 0;
    },
    isNullOrEmpty: function(val) {
      return this.isEmpty(val) && this.isBlank(val);
    },
    isEmpty: function(val) {
      return (!val || 0 === val.length);
    },
    isBlank: function(val) {
      return (!val || /^\s*$/.test(val));
    }
  };
  return _instance;
});