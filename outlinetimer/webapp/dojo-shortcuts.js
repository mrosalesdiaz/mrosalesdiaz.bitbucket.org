/***************************************
 * File: dojo-shortcuts.js
 *
 * Dojo shortcuts for speed typing
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
/***************************************
 * File: lyricsIndexController.js
 *
 * Controller for list lyrics page
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/lang',
], function(lang) {

  // function shortcuts
  window.FN_DUMMY = function() {};
  window.FN_ERROR_DUMMY = function() {
    AppEvents.emit("page-error", {
      message: "Error removing Lyric"
    });
  };
  window.h = lang.hitch;
  window.p = function(context, fn, params) {
    params = Array.prototype.slice.apply(arguments, [2])
    return lang.partial.apply(null, [lang.hitch(context, fn)].concat(params))
  };
  // end function shortcuts

});