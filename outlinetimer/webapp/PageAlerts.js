/***************************************
 * File: PageAlerts.js
 *
 * Notification service for the page. 
 * It listens for events and trigger 
 * notifications(alerts,etc)
 *
 * <code>
 *    AppEvents.emit('page-info',{message:"Hello"});
 * </code>
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojox/mvc/getStateful',
  'dojo/on',
  'dojo/domReady!'
], function(declare, lang, getStateful, on) {
  /****** Controller *****/
  var _controller = {
    __closeTimer: null,
    // page model
    model: getStateful({
      // search text input
      title: "",
      message: ""
    }),
    // startup lifecycle
    startup: function() {
      on(AppEvents, 'page-error', lang.hitch(this, this.__onPageError));
      on(AppEvents, 'page-info', lang.hitch(this, this.__onPageInfo));
    },
    __onPageInfo: function(data) {
      alert(data.message || "no_message");
    },
    __onPageError: function(err) {
      window.clearTimeout(this.__closeTimer);
      $(".page-alert").show();
      this.__closeTimer = window.setTimeout(function() {
        console.log("done")
        $(".page-alert").hide();
      }, 10000);
    }
  };
  // Register Global Events
  AppEvents.registerDefaultEvents(_controller);
  // AppEvents.on('event-name', lang.hitch( _controller, ( _controller.fnName || function() {} ) ) );
  /***********************/
  return _controller;
});