/***************************************
 * File: CSSClassBind.js
 *
 * Class to handle CSS class binding with dojo MVC 
 * 
 * <code type="html">
 *    <div class="col-xs-12 hide" 
 *       data-dojo-type="Element" 
 *       data-dojo-props="_setValueAttr: CSSClassBind.classSetter, value: at(controller.classes, 'step-1').equals(CSSClassBind.alwaysFalse)">
 * </code>
 * <code type="js-def-attributes">
 *    classes: getStateful({
 *      hide: ''
 *    }),
 * </code>
 * <code type="js-function">
 *    this.classes.set('step-1', '-hide');
 *    this.classes.set('step-2', '+hide');
 *    this.classes.set('step-3', '+hide');
 * </code>
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/lang',
  'dojo/dom-class',
  'dojo/domReady!'
], function(lang, domClass) {
  var _controller = {
    classSetter: function(cssClass) {
      // skip if value is undefined or null
      if (typeof(cssClass) === "undefined" || cssClass === null) return;
      cssClass = String(cssClass);
      var startChart;
      startChart = cssClass.charAt(0);
      cssClass = cssClass.substring(1);
      if (startChart === '*') {
        domClass.toggle(this.domNode, cssClass);
      } else if (startChart === '-') {
        domClass.remove(this.domNode, cssClass);
      } else if (startChart === '+') {
        domClass.add(this.domNode, cssClass);
      } else {
        domClass.replace(this.domNode, cssClass);
      }
    },
    alwaysFalse: function(value) {
      return false;
    }
  };
  return _controller;
});