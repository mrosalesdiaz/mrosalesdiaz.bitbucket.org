/***************************************
 * File: reveal-utils.js
 *
 * Utilities for Reveal initialization
 *
 * @since 2017.03.08
 * @author Maximo Rosales
 ***************************************/
define([
  'dojo/_base/lang'
], function(lang) {
  var _instance = {
    sync: function(slideNumberToMove) {
      Reveal.sync();
      if (arguments.length > 0 && !isNaN(slideNumberToMove)) {
        Reveal.slide(slideNumberToMove);
      }
    },
    slide: function(slideNumberToMove) {
      if (!isNaN(slideNumberToMove)) {
        Reveal.slide(slideNumberToMove);
      }
    },
    getCurrentIndex: function() {
      return Reveal.getIndices().h;
    },
    getCurrentSlide: function() {
      return Reveal.getCurrentSlide();
    },
    setBackground: function(path) {
      Reveal.configure({
        parallaxBackgroundImage: path
      });
    },
    initReveal: function() {

      Reveal.initialize({
        // Parallax background image
        parallaxBackgroundImage: '/images/bg_example.jpg',

        // Parallax background size
        parallaxBackgroundSize: '1000px', // CSS syntax, e.g. "2100px 900px"
        progress: false,
        embedded: true,
        center: false,
        keyboard: false,
        controls: false,
        // The "normal" size of the presentation, aspect ratio will be preserved
        // when the presentation is scaled to fit different resolutions. Can be
        // specified using percentage units.
        width: 960,
        height: 700,

        // Factor of the display size that should remain empty around the content
        //margin: 0.1,

        // Bounds for smallest/largest possible scale to apply to content
        //minScale: 0.2,
        //maxScale: 1.5,
        transition: 'slide', // none/fade/slide/convex/concave/zoom

        // More info https://github.com/hakimel/reveal.js#dependencies
        dependencies: [{
          src: '/reveal/lib/js/classList.js',
          condition: function() {
            return !document.body.classList;
          }
        }, {
          src: '/reveal/plugin/markdown/marked.js',
          condition: function() {
            return !!document.querySelector('[data-markdown]');
          }
        }, {
          src: '/reveal/plugin/markdown/markdown.js',
          condition: function() {
            return !!document.querySelector('[data-markdown]');
          }
        }, {
          src: '/reveal/plugin/highlight/highlight.js',
          async: true,
          callback: function() {
            hljs.initHighlightingOnLoad();
          }
        }, {
          src: '/reveal/plugin/zoom-js/zoom.js',
          async: true
        }, {
          src: '/reveal/plugin/notes/notes.js',
          async: true
        }]
      });
    }
  };
  return _instance;
});